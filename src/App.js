import React from 'react';
import { ProductTable } from "./components/ProductTable";
class SearchBar extends React.Component {
  constructor(props) { 
    super(props);
    this.handleFilterTextChange = this.handleFilterTextChange.bind(this);
    this.handleStockChange = this.handleStockChange.bind(this);
  }

  handleFilterTextChange(e) {
    this.props.onFilterTextChange(e.target.value);
  }

  handleStockChange(e) {
    this.props.onStockChange(e.target.checked );
  }

  render() {
    return(
      <form>
        <input type="text" value= {this.props.filterText} onChange= {this.handleFilterTextChange} placeholder="Search.." />
        <p>
          <input type="checkbox" checked= {this.props.inStockOnly} onChange= {this.handleStockChange} />
          {' '}
          Only Show Products in Stock.
        </p>
      </form>
    );
  }
}


export const PRODUCTS = [
  {category: 'Sporting Goods', price: '$49.99', stocked: true, name: 'Football'},
  {category: 'Sporting Goods', price: '$9.99', stocked: true, name: 'Baseball'},
  {category: 'Sporting Goods', price: '$29.99', stocked: false, name: 'Basketball'},
  {category: 'Electronics', price: '$99.99', stocked: true, name: 'iPod Touch'},
  {category: 'Electronics', price: '$399.99', stocked: false, name: 'iPhone 5'},
  {category: 'Electronics', price: '$199.99', stocked: true, name: 'Nexus 7'}
];

export class FilterableProductTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      filterText: '',
      inStockOnly: false
    };
    
    this.handleFilterTextChange = this.handleFilterTextChange.bind(this);
    this.handleStockChange = this.handleStockChange.bind(this);
  }
  
  handleFilterTextChange(filterText) {
    this.setState({
      filterText: filterText
    });
  }
  
  handleStockChange(inStockOnly) {
    this.setState({
      inStockOnly: inStockOnly
    })
  }
  
  // handleStockChange(inStockOnly) { 
  //   this.setState({
  //     inStockonly: inStockOnly
  //   })
  // }
  
    
  render() {
    return (
      <div>
        <SearchBar
          filterText={this.state.filterText}
          inStockOnly={this.state.inStockOnly}
          onFilterTextChange={this.handleFilterTextChange}
          onStockChange={this.handleStockChange}
        />
        <ProductTable
          products={this.props.products}
          filterText={this.state.filterText}
          inStockOnly={this.state.inStockOnly}
        />
      </div>
    );
  }
}